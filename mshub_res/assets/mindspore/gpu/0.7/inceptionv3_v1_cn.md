# InceptionV3

---

模型名称： InceptionV3

骨干网络： InceptionV3

模块类型：cv-classification

可微调：True

输入形状： [224, 224, 3]

模型版本：1.0

准确率： 0.788

作者：MindSpore团队

更新时间：2020-9-21

代码仓链接：<https://gitee.com/mindspore/mindspore/tree/r0.7/model_zoo/official/cv/inceptionv3>。

用户ID：MindSpore

用途：推理

训练后端：GPU

推理后端：GPU

MindSpore版本：0.7

许可证：Apache 2.0

摘要：使用InceptionV3对1000个类进行分类。

---

## 简介

该MindSpore Hub模型使用码云上MindSpore ModelZoo中的InceptionV3实现，目录为[model_zoo/official/cv/inceptionv3](https://gitee.com/mindspore/mindspore/tree/r0.7/model_zoo/official/cv/inceptionv3)。

## 参考论文

1. Szegedy, Christian，Vanhoucke, Vincent，Ioffe, Sergey，Shlens, Jonathon，Wojna, Zbigniew. Rethinking the Inception Architecture for Computer Vision[J]. 2015.

## Disclaimer

MindSpore ("we") do not own any ownership or intellectual property rights of the datasets, and the trained models are provided on an "as is" and "as available" basis. We make no representations or warranties of any kind of the datasets and trained models (collectively, “materials”) and will not be liable for any loss, damage, expense or cost arising from the materials. Please ensure that you have permission to use the dataset under the appropriate license for the dataset and in accordance with the terms of the relevant license agreement. The trained models provided are only for research and education purposes.

To Dataset Owners: If you do not wish to have a dataset included in MindSpore, or wish to update it in any way, we will remove or update the content at your request. Please contact us through GitHub or Gitee. Your understanding and contributions to the community are greatly appreciated.

MindSpore is available under the Apache 2.0 license, please see the LICENSE file.