# ResNet101

---

model-name: resnet101

backbone-name: resnet101

module-type: cv-classification

fine-tunable: True

input-shape: [224, 224, 3]

model-version: 1.0

author: MindSpore team

update-time: 2020-09-23

repo-link: <https://gitee.com/mindspore/models/tree/master/official/cv/resnet>

user-id: MindSpore

used-for: inference

train-backend: ascend

infer-backend: ascend

mindspore-version: 1.0

license: Apache2.0

summary: resnet101 for image classification

---

## Introduction

This MindSpore Hub model uses the implementation of ResNet101 from the MindSpore model zoo on Gitee at model_zoo/official/cv/resnet.

More details please refer to the [MindSpore model zoo on Gitee](https://gitee.com/mindspore/models/blob/master/official/cv/resnet/README.md).

## Citation

1. Jie Hu, Li Shen, Samuel Albanie, Gang Sun, Enhua Wu. "Squeeze-and-Excitation Networks"

## Disclaimer

MindSpore ("we") do not own any ownership or intellectual property rights of the datasets, and the trained models are provided on an "as is" and "as available" basis. We make no representations or warranties of any kind of the datasets and trained models (collectively, “materials”) and will not be liable for any loss, damage, expense or cost arising from the materials. Please ensure that you have permission to use the dataset under the appropriate license for the dataset and in accordance with the terms of the relevant license agreement. The trained models provided are only for research and education purposes.

To Dataset Owners: If you do not wish to have a dataset included in MindSpore, or wish to update it in any way, we will remove or update the content at your request. Please contact us through GitHub or Gitee. Your understanding and contributions to the community are greatly appreciated.

MindSpore is available under the Apache 2.0 license, please see the LICENSE file.